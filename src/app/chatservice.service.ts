import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

const baseURL="https://chat.twilio.com/v2/Services/";
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': 'Basic QUMyNTVhNGIwYWFmY2IxM2IxMjU2MDBkMGRmMTY1NzFjYzpkMTk1ZDE4NTU3NGNkODA5MWRlMDU1YWFjYzQ2ZmI5NQ=='
  })
};

@Injectable({
  providedIn: 'root'
})
export class ChatserviceService {
  constructor(public http: HttpClient) { }
  clientidentity: string = '1091452498703010437722';
  title = 'Chat-App';
  deleteURL = "https://chat.twilio.com/v2/Services/IS6998f3af9ff64da8a877ec2db47ea78d";
  Url = "https://chat.twilio.com/v2/Services";

  //function for post apicall 
  public postapicall(getURL:string,body:HttpParams): Observable < any > {
    let URL=baseURL+getURL;
    console.log(URL)
  return this.http.post(URL, body.toString(), httpOptions)
}
  //function for get apicall
 public getapicall(getURL: string): Observable < any > {
  let URL=baseURL+getURL
  return this.http.get(URL, httpOptions);
}
//   public CreateUserapicall()
// {
//   url =
//    return this.http.post()
// }
}