import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import {AuthService,GoogleLoginProvider} from 'angular-6-social-login';
import { HttpHeaders, HttpClient,HttpParams } from '@angular/common/http';
import {stringify} from 'querystring'
 import {ChatserviceService} from './chatservice.service'





@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  sid:string;
  message: string;
  recievemessage: string
  
  // title = 'Chat-App';
  // clientidentity:string='109145249870301043772';
  // Url="https://chat.twilio.com/v2/Services";

  constructor(private googleauthservice:AuthService,private chatservice:ChatserviceService){}
  
  public signin(googlePlatform: string)
  {
    let googlePlatformProvider;
    if(googlePlatform=="google"){
       googlePlatformProvider=GoogleLoginProvider.PROVIDER_ID;
    }
    this.googleauthservice.signIn(GoogleLoginProvider.PROVIDER_ID).then
    (
      (userdata)=>{
        console.log(googlePlatform+" data from google :",userdata)
      }
    );

  // var subs=this.apicall();
  //   subs.subscribe(data=>console.log(data));

  let url="";
  var subs=this.chatservice.getapicall(url);
  subs.subscribe(data=>{console.log(data.services[0].sid)
    this.sid=data.services[0].sid;
    console.log(this.sid);
  }
);
  }

  public showChannel()
  {
    let url="" +this.sid+"/Channels";
    var subs=this.chatservice.getapicall(url);
    subs.subscribe(data=>console.log(data));
    
  }

  public createChannel() {
    let friendly_name = "suhaas";
    let unique_name = "duke";
    let sendURL = "" + this.sid + "/Channels"
    let body = new HttpParams()
      .set('FriendlyName', friendly_name).set('UniqueName', unique_name);
    let subs = this.chatservice.postapicall(sendURL, body);
    subs.subscribe(data => console.log(data));
  }
  
  public JoinAChannel() {
    let friendlyname = "suhaas";
    let identity = "109145249870301043772";
    let channel_id = "CH7df31b74c1c842d1b824992a762a041f";
    let sendURL = "" + this.sid + "/Channels/" + channel_id + "/Members";
    let body = new HttpParams().set('Identity', identity).set('FriendlyName', friendlyname).set('ServiceSid', this.sid).set('ChannelSid', channel_id);
    let subs = this.chatservice.postapicall(sendURL, body);
    subs.subscribe(data => console.log(data));
  }

  public createUser() {
    let identity = "109145249870301043772";
    let friendlyname = "suhaas";
    let sendURL = "" + this.sid + "/Users";
    let body = new HttpParams().set('Identity', identity).set('FriendlyName', friendlyname).set('ServiceSid', this.sid);
    let subs = this.chatservice.postapicall(sendURL, body);
    subs.subscribe(data => console.log(data));
  }

  public sendMessage() {
    let channel_id = "CH7df31b74c1c842d1b824992a762a041f";
    let sendURL = "" + this.sid + "/Channels/" + channel_id + "/Messages";
    let body = new HttpParams().set('ServiceSid', this.sid).set('ChannelSid', channel_id).set('Body', this.message);
    let subs = this.chatservice.postapicall(sendURL, body);
    subs.subscribe(data => console.log(data));
  }

  

}
 
